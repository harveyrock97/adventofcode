#include "pch.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;

class AdventTools
{
public:
	AdventTools();
	~AdventTools();

	static vector<int> GetIntArrayFromFile(string);
};

