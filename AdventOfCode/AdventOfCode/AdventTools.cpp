#include "pch.h"
#include "AdventTools.h"

AdventTools::AdventTools()
{
}


AdventTools::~AdventTools()
{
}

vector<int> AdventTools::GetIntArrayFromFile(string filePath)
{
	vector<int> numbers;

	ifstream in(filePath, ios::in);
	if (!in) {
		cout << "Unable to open file " + filePath;
		return numbers;
	}

	int number;
	while (in >> number) {
		numbers.push_back(number);
	}

	in.close();

	return numbers;
}